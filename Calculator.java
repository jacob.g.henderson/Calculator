
/**
 * Write a description of class Calculator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

public class Calculator
{
    // instance variables - replace the example below with your own
    private float numberOne;
    private float numberTwo;

    /**
     * Constructor for objects of class Calculator
     */
    public Calculator(float one, float two)
    {
        // initialise instance variables
        numberOne = one;
        numberTwo = two;
    }

    /**
     * For adding the numbers given to the calculator
     */
    public float add()
    {
        // put your code here
        float result = numberOne + numberTwo;
        // System.out.println(numberOne + " plus " + numberTwo + " = " + result);
        return result;
    }
    
    public float subtract()
    {
        float result = numberOne - numberTwo;
        return result;
    }
    
    public float divide()
    {
        float result = numberOne / numberTwo;
        return result;
    }

    public float multiply()
    {
        float result = numberOne * numberTwo;
        return result;
    }
    
    public float square()
    {
        float result = numberOne * numberOne;
        return result;
    }
    
    public float getNumberOne()
    {
        return numberOne;
    }
    
    public float getNumberTwo()
    {
        return numberTwo;
    }
    
    //fraction
    
    //percentage
    
    //square root
    
    //tan cos sin - inverse
    
    //deg -> rad
    
    //powers
    
    //factorial!
    
    //logs base 
    
    
    
    
    
}
